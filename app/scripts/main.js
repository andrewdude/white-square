// Main slider func
$('.main-slider').slick({
	autoplay: true,
	autoplaySpeed: 3000,
	infinite: true,
	speed: 1000,
	fade: true,
	arrows: false,
	cssEase: 'linear'
});

// Mobile nav func
$(function ()
{
	var $window = $(window),
			$nav = $('#headerNav'),
			$button = $('#toggleNav'),
			$buttonInside = $('#headerNav li a');


	$button.on('click', function () {
		$nav.slideToggle();
		$(this).toggleClass('opened');
	});

	if ($window.width() < 752) {
		$buttonInside.on('click', function () {
			$nav.slideUp();
			$button.toggleClass('opened');
		});
	}
	
	$window.on('resize', function () {
		if ($window.width() > 752) {
			$nav.show();
		} else {
			$nav.hide();

			$buttonInside.on('click', function () {
				$nav.slideUp();
				$button.toggleClass('opened');
			});

		}
	});
});

// Scroll to func
$('a[href^="#go"]').click(function(){
		$('html, body').animate({
				scrollTop: $( $(this).attr('href') ).offset().top - 60
		}, 500);
		return false;
});

$(window).scroll(function(){
  var headerMenu = $('.header-menu'),
      scroll = $(window).scrollTop();

  if (scroll >= 135) {
		headerMenu.addClass('fixed');
	}
  else {
		headerMenu.removeClass('fixed');
	};
});